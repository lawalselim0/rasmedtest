var createError = require("http-errors");
const express = require("express");
const path = require("path");
let port = 4000;
const cookieParser = require("cookie-parser");
const mongoose = require("mongoose");
const keys = require("./keys.js");
const indexRouter = require("./routes/index");
const router = require("express").Router();

const app = express();

mongoose.Promise = global.Promise;
mongoose
  .connect(keys.mongodb.dbURI, {
    useNewUrlParser: true,
  })
  .then(
    //useNewUrlParser: true,
    function (res) {
      console.log("Connected to Database Successfully.");
    }
  )
  .catch(function (err) {
    console.log("Connection to Database failed.");
    console.log(err);
  });

app.use((req, res, next) => {
  console.log(`${new Date().toString()} => ${req.originalUrl}`);
  next();
});

// view engine setup

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.set("layout");

const expressLayouts = require("express-ejs-layouts");

// user router
app.use(expressLayouts);
app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

app.listen(port, () => {
  console.log(`listening to server on port ${port}`);
});
// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error", {
    layout: "layout",
  });
});

module.exports = app;
