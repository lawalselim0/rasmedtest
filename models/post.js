const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var post = new Schema({
  title: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  category: { type: Array, default: [] },
  author: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Post", post);
