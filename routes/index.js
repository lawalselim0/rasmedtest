var express = require("express");
var router = express.Router();
var Post = require("../models/post");

// Get landing page with data limit of 4
router.get("/", function (req, res, next) {
  var perPage = 4;
  var page = req.params.page || 1;

  Post.find({})
    .skip(perPage * page - perPage)
    .limit(perPage)
    .exec(function (err, posts) {
      Post.count().exec(function (err, count) {
        if (err) return next(err);
        res.render("main/homepage", {
          posts: posts,
          current: page,
          pages: Math.ceil(count / perPage),
        });
      });
    });
});

// Renders homepage content
router.get("/homepage/:page", (req, res, next) => {
  var perPage = 4;
  var page = req.params.page || 1;

  Post.find({})
    .skip(perPage * page - perPage)
    .limit(perPage)
    .exec(function (err, posts) {
      Post.count().exec(function (err, count) {
        if (err) return next(err);
        res.render("main/homepage", {
          posts: posts,
          current: page,
          pages: Math.ceil(count / perPage),
        });
      });
    });
});

// Post Request TO the database
router.post("/post", (req, res, next) => {
  const post = new Post({
    title: req.body.title,
    author: req.body.author,
    category: req.body.category,
    content: req.body.content,
  });
  post
    .save()
    .then((result) => {
      console.log(result);
      res.render("main/newblog", { message: "Post Saved" });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.get("/newblog", (req, res, next) => {
  res.render("main/newblog", {
    title: "New blog",
  });
});

module.exports = router;
