# Rasmedtest

Simple Blog app with pagination

Steps to test
N.B Ensure you have node.js running globally on your machine if not download the latest version here:https://nodejs.org/en/download/

Step 1: Clone the repo from: https://gitlab.com/lawalselim0/rasmedtest.git into any folder of your choice on your local machine.

Step 2: Run npm install or npm i to install the development packages

step 3:Run npm start from the ide terminal of your choice

if you are using windows terminal cd into the project directory and run npm start

step 4: go to your favourite browser and type in localhost:4000/ ensure no other program is running on the same port [4000]
